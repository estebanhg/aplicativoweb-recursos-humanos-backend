const Auth={}
const jwt=require('jsonwebtoken')

Auth.verificartoken=(req,res,next)=> {
    if(!req.headers.autorizacion){
        return res.json({
            mensaje:'No estas autorizado'
        })
    }
    // Obtener Toke de beares token --> separamos por el espaciado
    const token=req.headers.autorizacion.split(' ')[1]
    //Verificar si el Token esta vacio
    if(token==='null'){
        return res.json({
            mensaje:'No estas autorizado'
        })
    }
    const payload=jwt.verify(token,'RH')
    req.userid=payload._id
    next()
}

module.exports=Auth