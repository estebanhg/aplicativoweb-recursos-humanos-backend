const {Router}=require('express')
const router=Router()
const ImagenCtrl=require('../controllers/Imagen.controller')
const Upload=require('../libs/Upload')

router.post('/img',Upload.single('image'),ImagenCtrl.addProduct)

module.exports=router