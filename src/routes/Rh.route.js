const {Router}=require('express')
const router=Router()
const RhCtrl=require('../controllers/Rh.controller')
const Auth=require('../helpers/Aunt')



router.get('/rh',Auth.verificartoken,RhCtrl.listar)
router.get('/rh/buscar/:name',Auth.verificartoken,RhCtrl.BuscarName)
router.post('/rh/registrar',Auth.verificartoken,RhCtrl.registrar)
router.post('/rh/login',RhCtrl.login)
router.delete('/rh/eliminar/:id',Auth.verificartoken,RhCtrl.eliminarusuario)
router.put('/rh/actualizar/:id',Auth.verificartoken,RhCtrl.actualizar)






module.exports=router