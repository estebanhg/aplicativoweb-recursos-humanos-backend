const {Router}=require('express')
const router=Router()
const AdminCtrl=require('../controllers/Admin.controller')

router.get('/admin',AdminCtrl.listar)
router.post('/admin/registrar',AdminCtrl.registrar)
router.post('/admin/login',AdminCtrl.login)


module.exports=router