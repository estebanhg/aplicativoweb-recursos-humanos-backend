const {Router}=require('express')
const router=Router()
const EmpCtrl=require('../controllers/Empleados.controller')
const auth=require('../helpers/Aunt')



router.get('/empleados',auth.verificartoken,EmpCtrl.listarEmpleados)
router.get('/empleados/buscar/:name',auth.verificartoken,EmpCtrl.BuscarName)
router.get('/empleadoscontratistas/:id',auth.verificartoken,EmpCtrl.listarConContratista)
router.get('/empleados/oneUser/:id',auth.verificartoken,EmpCtrl.listarID)
router.post('/empleados/registrar',auth.verificartoken,EmpCtrl.RegistrarEmpleado)
router.post('/empleados/login',EmpCtrl.login)
router.delete('/empleados/eliminar/:id',auth.verificartoken,EmpCtrl.eliminar)
router.put('/empleados/actualizar/:id',auth.verificartoken,EmpCtrl.actualizarEmp)

module.exports=router