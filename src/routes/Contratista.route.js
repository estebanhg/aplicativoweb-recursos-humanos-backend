const {Router}=require('express')
const router=Router()
const ContCtrl=require('../controllers/Contratista.controller')
const auth=require('../helpers/Aunt')



router.get('/contratistas',auth.verificartoken,ContCtrl.listar)
router.get('/contratistas/buscar/:name',auth.verificartoken,ContCtrl.BuscarName)
router.get('/contratistas/obtener/:id',auth.verificartoken,ContCtrl.listarID)
router.post('/contratistas/registrar',auth.verificartoken,ContCtrl.registrar)
router.delete('/contratistas/eliminar/:id',auth.verificartoken,ContCtrl.eliminarusuario)
router.put('/contratistas/actualizar/:id',auth.verificartoken,ContCtrl.actualizarusuario)
router.post('/contratistas/login',ContCtrl.login)

module.exports=router