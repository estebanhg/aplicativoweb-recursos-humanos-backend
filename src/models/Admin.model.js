const mongoose=require('mongoose')
const {Schema} = mongoose
//Encriptar password
const bcrypt=require('bcryptjs')

const AdminSchema=new Schema({
    name: String,
    lastname: String,
    cc: String,
    email: String,
    password: String
},{
    //Fecha de creacion
    timestamps: true
}
)

//Metodo para encriptar
AdminSchema.methods.encryptarPassword=async password=>{
    //Palabra secreta 10 vueltas
    const salt=await bcrypt.genSalt(10)

    //Encriptar password con la palabra clave
    return await bcrypt.hash(password,salt)
}

module.exports=mongoose.model('admins',AdminSchema) 