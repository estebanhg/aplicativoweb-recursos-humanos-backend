const mongoose=require('mongoose')
const {Schema} = mongoose
//Encriptar password
const bcrypt=require('bcryptjs')

const ImagenSchema= new Schema({
    titulo:String,
    descripcion:String,
    imageUrl:String
})

//Metodo para convertir imagen en URL
ImagenSchema.methods.SetImgUrl=function SetImgUrl(filename){
    const url='http://localhost:4000/'
    this.imageUrl=url+'public/'+filename
}

module.exports=mongoose.model('imgs',ImagenSchema)