const mongoose=require('mongoose')
const {Schema} = mongoose
//Encriptar password
const bcrypt=require('bcryptjs')

const EmpleadoSchema=new Schema({
    name: String,
    lastname: String,
    cc: String,
    telefono: String,
    contrato: String,
    adress: String,
    nacimiento: Date,
    email: String,
    password: String,
    contratistas:{type:Schema.Types.ObjectId,ref:'contratistas'},
    imageUrl: String
},{
    //Fecha de creacion
    timestamps: true
}
)

//Metodo para encriptar
EmpleadoSchema.methods.encryptarPassword=async password=>{
    //Palabra secreta 10 vueltas
    const salt=await bcrypt.genSalt(10)

    //Encriptar password con la palabra clave
    return await bcrypt.hash(password,salt)
}

module.exports=mongoose.model('empleados',EmpleadoSchema) 