const mongoose=require('mongoose')
const {Schema} = mongoose
//Encriptar password
const bcrypt=require('bcryptjs')

const RhSchema=new Schema({
    name: String,
    lastname: String,
    cc: String,
    email: String,
    password: String,
    imageUrl:String
},{
    //Fecha de creacion
    timestamps: true
}
)


//Metodo para encriptar
RhSchema.methods.encryptarPassword=async password=>{
    //Palabra secreta 10 vueltas
    const salt=await bcrypt.genSalt(10)

    //Encriptar password con la palabra clave
    return await bcrypt.hash(password,salt)
}

//Metodo para convertir imagen en URL
RhSchema.methods.setImgUrl=function setImgUrl(filename){
    const url='http://localhost:4000/'
    this.imageUrl=url+'public/'+filename
}



module.exports=mongoose.model('admin',RhSchema) 