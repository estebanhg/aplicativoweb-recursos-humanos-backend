const ImagenCtrl={}
const Product=require('../models/Imagen.model')

ImagenCtrl.addProduct=async(req,res)=>{
    const {titulo,descripcion}=req.body
    const product=new Product({titulo,descripcion})
    if(req.file){
        const {filename}=req.file
        product.SetImgUrl(filename)
    }
    const productstorage=await product.save()
    res.json(productstorage)
}


module.exports=ImagenCtrl