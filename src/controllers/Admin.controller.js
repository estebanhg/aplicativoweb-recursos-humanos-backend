const AdminCtrl ={}
const Admin=require('../models/Admin.model')
const bcrypt=require('bcryptjs')
const jwt=require('jsonwebtoken')

//Controlador para crear admin
AdminCtrl.registrar=async(req,res)=>{
    
    const {name,lastname,cc,email,password}=req.body

    //Verificar si el correo existe o no 
    const correousuario=await Admin.findOne({email:email})
    if (correousuario){
        res.json({
            mensaje: 'El correo la existe'
        })
    }else{
        //Entonces mandamos el usuario
        const nuevousuario= new Admin({name,lastname,cc,email,password})
        nuevousuario.password=await nuevousuario.encryptarPassword(password)
        await nuevousuario.save()
        //Generar codigo de barra unico o Token unico
        const token=jwt.sign({_id:nuevousuario._id,name:nuevousuario.name},'RH')
        const usuario=await Admin.findOne({email:email})
        res.json({
            mensaje:'Bienvenido',
            id:usuario._id,
            token
        })


    }

}


// Controlador para el LOGIN de Admin
AdminCtrl.login=async(req,res)=>{

    const {email,password}=req.body

    //Verificamos que exista el usuario -> devuele todos los campos 
    const usuario=await Admin.findOne({email: email})
    if(!usuario){
        return res.json({
            mensaje: 'Correo/Contraseña incorrecta'
        })
    }bcrypt.compare(password,usuario.password,function(err,resp){
        if(resp){
            const token=jwt.sign({ _id: usuario._id,name: usuario.name},'RH')
            res.json({
                mensaje: 'Bienvenido',
                id: usuario._id,
                name: usuario.name,
                email:usuario.email,
                rol: 'Administrador',
                imageUrl: undefined,
                token
            })
        }else {
            return res.json({
                mensaje: 'Contraseña/Correo incorrecto'
            })
        } 
    })
}

//Controlador para listar Admin
AdminCtrl.listar=async(req,res)=>{
    const usuarios=await Admin.find({},{password:0})
    res.json(usuarios)
}



module.exports=AdminCtrl