const RhCtrl ={}
const Rh=require('../models/Rh.model')
const bcrypt=require('bcryptjs')
const jwt=require('jsonwebtoken')



//Controlador para listar usuarios
RhCtrl.listar=async(req,res)=>{
    const usuarios=await Rh.find({},{password:0})
    res.json(usuarios)
}



//Controlador para el registro
RhCtrl.registrar=async(req,res)=>{
    const {name,lastname,cc,email,password}=req.body

    //Verificar si el correo existe o no 
    const correousuario=await Rh.findOne({email:email})
    if (correousuario){
        res.json({
            mensaje: 'El correo ya existe'
        })
    }else{
        //Entonces mandamos el usuario
        const nuevousuario= new Rh({name,lastname,cc,email,password})
        nuevousuario.password=await nuevousuario.encryptarPassword(password)
        await nuevousuario.save()
        //Generar codigo de barra unico o Token unico
        const token=jwt.sign({_id:nuevousuario._id,name:nuevousuario.name},'RH')
        const usuario=await Rh.findOne({email:email})
        res.json({
            mensaje:'Bienvenido',
            id:usuario._id,
            token
        })


    }

}

//Controlador con peticion DELETE 
RhCtrl.eliminarusuario=async(req,res)=>{
    const id=req.params.id
    //Corroborar de que el usuario exista
    const usuario=await Rh.findById({_id:id})
    if(!usuario){
        return res.json({
            mensaje:"El usuario no existe"
        })
    }
    await Rh.findOneAndDelete({_id:id})
    res.json({
        mensaje:"Usuario eleminado correctamente"
    })

}

//Actualizar
RhCtrl.actualizar=async(req,res)=>{
    const id=req.params.id

    //Corroborar de que el usuario exista
    const usuario=await Rh.findById({_id:id})
    if(!usuario){
        return res.json({
            mensaje:"El usuario no existe"
        })
    }
    await Rh.findByIdAndUpdate({_id:id},req.body)
    res.json({
        mensaje: "Usuario actualizado correctamente"
    })
}


// Controlador para el LOGIN
RhCtrl.login=async(req,res)=>{

    const {email,password}=req.body
    //Verificamos que exista el usuario -> devuele todos los campos 
    const usuario=await Rh.findOne({email: email})
    if(!usuario){
        return res.json({
            mensaje: 'Correo/Contraseña incorrecta'
        })
    }bcrypt.compare(password,usuario.password,function(err,resp){
        if(resp){
            const token=jwt.sign({ _id: usuario._id,name: usuario.name},'RH')
            res.json({
                mensaje: 'Bienvenido',
                id: usuario._id,
                name: usuario.name,
                email: usuario.email,
                rol: 'RH',
                imageUrl:usuario.imageUrl,
                token
            })
        }else {
            return res.json({
                mensaje: 'Contraseña/Correo incorrecto'
            })
        } 
    })
}
RhCtrl.BuscarName=async(req,res)=>{
    const name=req.params.name
    const resultados=await Rh.find({name:{$regex:'.*'+name}},{password:0})
    res.json(resultados)

}


module.exports=RhCtrl