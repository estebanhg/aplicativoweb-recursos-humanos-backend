const EmpCtrl = {}
const Emp=require('../models/Empleados.model')
const bcrypt=require('bcryptjs')
const jwt=require('jsonwebtoken')

//Controlador Listar Todos
EmpCtrl.listarEmpleados=async(req,res)=>{

    //Vamos a hacer le Join o pegado de colecciones con --> populate()
    const usuarios=await Emp.find({},{password:0}).populate('contratistas',{password:0})
    res.json(usuarios)
}

//Listar por nombre
EmpCtrl.BuscarName=async(req,res)=>{
    const name=req.params.name
    const resultados=await Emp.find({name:{$regex:'.*'+name}},{password:0})
    res.json(resultados)

}

EmpCtrl.listarID=async(req,res)=>{
    const id=req.params.id
    const empleado=await Emp.find({_id:id},{password:0})
    res.json(empleado)

}

//Controlador mostrar empelados contratistas
EmpCtrl.listarConContratista=async(req,res)=>{
    const id=req.params.id
    //const id=req.userid //userid --> donde queda registrado el ID 
    const empleados=await Emp.find({contratistas:id},{password:0}).populate('contratistas',{password:0})
    res.json(empleados)
}

EmpCtrl.RegistrarEmpleado=async(req,res)=>{
    const {name,lastname,cc,telefono,contrato,adress,nacimiento,email,password,contratistas} = req.body
    

    const correousuario=await Emp.findOne({email:email})
    if (correousuario){
        res.json({
            mensaje: 'El correo ya existe'
        })
    }else{
        const empleado= new Emp({
            name : name,
            lastname : lastname,
            cc: cc,
            telefono: telefono,
            contrato: contrato,
            adress: adress,
            nacimiento: nacimiento,
            email: email,   
            password: password,
            contratistas:contratistas
        })

        empleado.password=await empleado.encryptarPassword(password)
        await empleado.save()

        const token=jwt.sign({_id:empleado._id,name:empleado.name},'RH')
        const usuario=await Emp.findOne({email:email})
        res.json({
            mensaje:'Bienvenido',
            id:usuario._id,
            token
        })
    }
    
    res.json({
        mensaje: 'Empleado registrado correctamente'
    })
}

EmpCtrl.eliminar=async(req,res)=>{
    const id=req.params.id

    //Validad errores
    const empEliminado=await Emp.findById({_id:id})
    if(!empEliminado){
        return res.json({
            mensaje:'El empleado no existe'
        })
    } 
    await Emp.findOneAndDelete({_id:id})
    res.json({
        mensaje:'Eliminado!'
    })
}

EmpCtrl.actualizarEmp=async(req,res)=>{
    const id=req.params.id
    const empActualizar=await Emp.findById({_id:id})
    if(!empActualizar){
        return res.json({
            mensaje: 'El empleado no existe'
        })
    }
    await Emp.findByIdAndUpdate({_id:id},req.body)
    res.json({
        mensaje:'Actualizado!'
    })
}

// Controlador para el LOGIN
EmpCtrl.login=async(req,res)=>{

    const {email,password}=req.body
    //Verificamos que exista el usuario -> devuele todos los campos 
    const usuario=await Emp.findOne({email: email})
    if(!usuario){
        return res.json({
            mensaje: 'Correo/Contraseña incorrecta'
        })
    }bcrypt.compare(password,usuario.password,function(err,resp){
        if(resp){
            const token=jwt.sign({ _id: usuario._id,name: usuario.name},'RH')
            res.json({
                mensaje: 'Bienvenido',
                id: usuario._id,
                name: usuario.name,
                email:usuario.email,
                rol: 'Empleado',
                imageUrl: usuario.imageUrl,
                token
            })
        }else {
            return res.json({
                mensaje: 'Contraseña/Correo incorrecto'
            })
        } 
    })
}
    





module.exports=EmpCtrl