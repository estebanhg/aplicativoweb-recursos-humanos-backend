const ContCtrl ={}
const Cont=require('../models/Contratista.model')
const bcrypt=require('bcryptjs')
const jwt=require('jsonwebtoken')


//Controlador para listar usuarios
ContCtrl.listar=async(req,res)=>{
    const usuarios=await Cont.find({},{password:0})
    res.json(usuarios)
}

//Controlador para listar usuario unico por Nombre
ContCtrl.listarName=async(req,res)=>{
    const name=req.params.name
    const usuario=await Cont.find({name: name},{password:0})
    res.json(usuario)
}

//Listar usuario unico por ID 
ContCtrl.listarID=async(req,res)=>{
    const id=req.params.id
    const usuario=await Cont.find({_id: id})
    res.json(usuario)
}

//Controlador para el registro
ContCtrl.registrar=async(req,res)=>{
    const {name,lastname,cc,telefono,empresa,email,password}=req.body

    //Verificar si el correo existe o no 
    const correousuario=await Cont.findOne({email:email})
    if (correousuario){
        res.json({
            mensaje: 'El correo la existe'
        })
    }else{
        //Entonces mandamos el usuario
        const nuevousuario= new Cont({name,lastname,cc,telefono,empresa,email,password})
        nuevousuario.password=await nuevousuario.encryptarPassword(password)
        await nuevousuario.save()
        //Generar codigo de barra unico o Token unico
        const token=jwt.sign({_id:nuevousuario._id,name:nuevousuario.name},'RH')
        const usuario=await Cont.findOne({email:email})
        res.json({
            mensaje:'Bienvenido',
            id:usuario._id,
            token
        })


    }

}

//Controlador con peticion DELETE 
ContCtrl.eliminarusuario=async(req,res)=>{
    const id=req.params.id
    //Corroborar de que el usuario exista
    const usuario=await Cont.findById({_id:id})
    if(!usuario){
        return res.json({
            mensaje:"El usuario no existe"
        })
    }
    await Cont.findOneAndDelete({_id:id})
    res.json({
        mensaje:"Usuario eleminado correctamente"
    })

}

//Controlador con peticion PUT Actualizar
ContCtrl.actualizarusuario=async(req,res)=>{
    const id=req.params.id

    //Corroborar de que el usuario exista
    const usuario=await Cont.findById({_id:id})
    if(!usuario){
        return res.json({
            mensaje:"El usuario no existe"
        })
    }
    await Cont.findByIdAndUpdate({_id:id},req.body)
    res.json({
        mensaje: "Usuario actualizado correctamente"
    })
}


// Controlador para el LOGIN
ContCtrl.login=async(req,res)=>{

    const {email,password}=req.body
    //Verificamos que exista el usuario -> devuele todos los campos 
    const usuario=await Cont.findOne({email: email})
    if(!usuario){
        return res.json({
            mensaje: 'Correo/Contraseña incorrecta'
        })
    }bcrypt.compare(password,usuario.password,function(err,resp){
        if(resp){
            const token=jwt.sign({ _id: usuario._id,name: usuario.name},'RH')
            res.json({
                mensaje: 'Bienvenido',
                id: usuario._id,
                name: usuario.name,
                rol: 'Contratista',
                email:usuario.email,
                imageUrl:usuario.imageUrl,
                token
            })
        }else {
            return res.json({
                mensaje: 'Contraseña/Correo incorrecto'
            })
        } 
    })
}

ContCtrl.BuscarName=async(req,res)=>{
    const name=req.params.name
    const resultados=await Cont.find({name:{$regex:'.*'+name}},{password:0})
    res.json(resultados)

}

module.exports=ContCtrl