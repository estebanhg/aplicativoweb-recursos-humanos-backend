const multer=require('multer')
const path=require('path')
const {v4:uuidv4}=require('uuid')


//Funcion para guardar las imagenes
const storage = multer.diskStorage({
    destination: path.join(__dirname,'../storage/imgs'),
    filename: function (req, file, cb) {
      cb(null, uuidv4()+path.extname(file.originalname).toLowerCase())
    }
})
// funcion para subir las imagenes
const upload = multer({ 
    storage,
    fileFilter:(req,file,cb)=>{
        const filetypes=/jpeg|jpg|png|gif/
        const mimetype=filetypes.test(file.mimetype)
        const extname=filetypes.test(path.extname(file.originalname))
        if(mimetype && extname){
            return cb(null,true)
        }
        cb('error: el archivo debe ser una imagen válida')
    }
})

module.exports=upload